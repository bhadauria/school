from django.db import models
from django.contrib.auth.models import BaseUserManager,AbstractBaseUser
class AccountManager(BaseUserManager):
	def create_user(self,email,password=None):
		if not email:
			raise ValueError("Email is requred")
		account = self.model(
		email = self.normalize_email(email),
		)		
		account.set_password(password)
		account.save(using=self._db)
		return account
	def create_superuser(self,email,password):
		account = self.create_user(email=email,password=password)
		account.is_admin = True	
		account.save(using=self._db)
		return account	

class MyUser(AbstractBaseUser):
	email = models.EmailField(max_length=255,unique=True)
	mobile=models.IntegerField(null=True,blank=True)
	name = models.CharField(max_length=100,null=True,blank=True)
	dob = models.DateField(null=True,blank=True)
	create_at = models.DateField(auto_now_add=True)
	is_active = models.BooleanField(default=False)
	is_admin = models.BooleanField(default=False)
	TYPE=(('student','student'),('teacher','teacher'))
	types=models.CharField(choices=TYPE,max_length=15,null=True,blank=True)
	is_teacher = models.BooleanField(default=False)
	is_student = models.BooleanField(default=False)
	objects = AccountManager()
	USERNAME_FIELD = 'email'
	REQUIRED_FIELD = ['email']

	def get_full_name(self):
		return self.email

	def get_short_name(self):
		return self.email
	def  __str__(self):
		return self.name
	def has_perm(self,perm,obj=None):
		return True		
	def has_module_perms(self,app_lebel):
		return True		
	
	def is_staff(self):
		return self.is_admin	