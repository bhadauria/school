# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import traceback,json
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from account.models import MyUser
from rest_framework.renderers import TemplateHTMLRenderer
from django.contrib.auth import authenticate
from rest_framework import status
from django.core import serializers
import smtplib
from email.mime.multipart import MIMEMultipart
from email.MIMEText import MIMEText
from school import settings

class Signup(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	template_name = 'myclass/register1.html'
	def post(self,request):
		try:
			data=request.data
			print (data)
			obj=MyUser.objects.create(email=data['email'],mobile=data['mobile'],types=data['type'])	
			obj.set_password(data['password'])
			obj.save()
			return Response({"data":"Thanks for registration and let wait for approve"})
		except Exception as e:
			traceback.print_exc()
			return Response({"kkk":"ll"})
class Login(APIView):
	renderer_classes = (TemplateHTMLRenderer,)
	def post(self,request):
		try:
			data=request.data
			print ("=========",data)
			user = authenticate(username=data['email'],password=data['password'])
			print ('>>>>>>>>>>>>>>>>>>',user)
			if user is not None:
				if user.is_admin:
					return Response({"name":user.name},template_name='myclass/admin.html')
				else:
					return Response({"name":user.name},template_name='myclass/sudent.html')		
			else:
				return Response({"data":"invalid email/password"},template_name='myclass/login.html')	
		except Exception as e:
			traceback.print_exc()
			return Response(template_name='myclass/admin.html')		
class RegistrationList(APIView):
	renderer_classes=(TemplateHTMLRenderer,)
	def get(self,request):
		try:
			name = request.GET.get('key')
			print ("fffffff",name)
			stud=MyUser.objects.filter(types="student",is_active=False)
			serialized_obj = json.dumps(serializers.serialize('json', stud,fields=('pk','name','is_active','email')))
			data=[]
			for i in stud:	
				data.append({'pk':i.pk,'name':i.name,"email":i.email,"mobile":i.mobile,"is_active":i.is_active})	
			return Response({"data":data,"name":name},template_name="myclass/admin1.html")
		except Exception as e:
			traceback.print_exc()
			return Response(status=400,template_name="myclass/admin.html")	
	def post(self,request):
		try:
			data=dict(request.data.iterlists())
			print ('1111111>>>>>',data,)
			pk_list = data['approve']
			print "33333333",pk_list
			obj=MyUser.objects.filter(pk__in=pk_list)
			print obj
			obj.update(is_active=True)
			to=[]
			# subject = 'email_verification'
			message= "\r\n".join([
			"Subject: Registration Complete",
			"Your Registration approved",
			"Click here to login",
			"http://127.0.0.1:8000/loginform"
			])
			for i in obj:
				to.append(i.email)
			s = smtplib.SMTP('smtp.gmail.com:587')
			s.ehlo()
			s.starttls()
			s.login(settings.from_mail,settings.passs)
			s.sendmail(settings.from_mail, to, message)
			s.close()
				
			return Response(template_name="myclass/admin.html")
		except Exception as e:
			traceback.print_exc()		
			return Response(status=400,template_name="myclass/admin.html")