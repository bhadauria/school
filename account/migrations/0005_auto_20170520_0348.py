# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-20 03:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_myuser_types'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='types',
            field=models.CharField(blank=True, choices=[(b'student', b'student'), (b'teacher', b'teacher')], max_length=15, null=True),
        ),
    ]
