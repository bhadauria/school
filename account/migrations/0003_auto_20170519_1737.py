# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-19 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20170519_1736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
