"""school URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from account import views
from myclass import views as classview
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/user/signup/$',views.Signup.as_view(),name='signup'),
    url(r'^home/$',classview.home, name="ff"),
    url(r'^form/$',classview.form, name="form"),
    url(r'^subjects',classview.subject,name="subject"),
    url(r'^course',classview.CourseView.as_view(),name="course"),
    url(r'^login/$',views.Login.as_view(),name="login"),
    url(r'^loginform',classview.login,name="loginform"),
    url(r'api/myclass',include('myclass.urls'),name="myclass"),
    url(r'api/user',views.RegistrationList.as_view(),name="registerlist"),
    url(r'^api/myclass/subject',classview.SubjectView.as_view(),name="addsubject"),
    url(r'^api/myclass/teacher',classview.TeacherView.as_view(),name="teacher"),
    url(r'api/teacherlist/',classview.TeacherListView.as_view(),name="teacherlist"),
    url(r'api/myclass/dashboard',classview.DashBoard.as_view(),name="dashboard")
]
