# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from myclass.models import *
admin.site.register(Subject)
admin.site.register(Designation)
admin.site.register(Teacher)
admin.site.register(Course)

# Register your models here.
