# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import traceback
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from account.models import MyUser
from myclass.models import *
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import status
# from rest_framework.decorators import api_view
from django.http import HttpResponse

class SubjectView(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	def post(self,request):
		try:
			data = request.data
			print ">>>>>>>>>:",data['subject']
			Subject.objects.get_or_create(name=data['subject'])
			return Response(template_name = 'myclass/subject.html',status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(status=400,template_name = 'myclass/admin.html')
	def get(self,request):
		try:
			print ">>>>>>>>>:get"
			sub=Subject.objects.all()
			data=[]
			for i in sub:
				data.append({"name":i.name})	
			return Response({"data":data},template_name = 'myclass/subject1.html',status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(status=400,template_name = 'myclass/admin.html')				
def home(request):
	print ("SSS")
	return render (request,'myclass/registration.html')
def form(request):
	print ("SSS")
	return render (request,'myclass/form.html')	
def login(request):
	print ("SSS")
	return render (request,'myclass/login.html')	
def subject(request):
	return render(request,'myclass/subject.html')
# @api_view(['GET', 'POST', ])
class CourseView(APIView):	
	renderer_classes=(TemplateHTMLRenderer,)		
	def get(self,request):
		try:
			subject=Subject.objects.all()
			data=[]
			for i in subject:
				data.append({"name":i.name,"pk":i.pk})
			return Response({"data":data},template_name='myclass/course.html')			
		except:
			return Response(status=400,template_name='myclass/course.html')				
	def post(self,request):
		try:
			data=dict(request.data.iterlists())
			print (">>>>>>>>",data,data['subject'])
			sub=Subject.objects.filter(pk__in=data['subject'])
			course=Course.objects.get_or_create(name=data['course'][0])
			for i in sub:
				course[0].subject.add(i)
				course[0].save()
				
			return Response(template_name="myclass/admin.html")	
		except Exception:
			traceback.print_exc()
			return Response(status=400,template_name='myclass/admin.html')		
class TeacherView(APIView):
	renderer_classes=(TemplateHTMLRenderer,)
	def get(self,request):
		try:
			course=Course.objects.filter()
			subject=Subject.objects.all()
			c=[]
			s=[]
			for i  in course:
				d=[]
				for j in i.subject.all():
					d.append({"name":j.name})
				c.append({"name":i.name,"pk":i.pk,"subject":d})
			for i in subject:
				s.append({"name":i.name,"pk":i.pk})	
			if request.GET.get('user')=="student":
				return Response({"data":c},template_name="myclass/student1.html")
			else:								
				return Response({"course":c,"sub":s},template_name="myclass/teacher.html")
		except Exception as e:
			traceback.print_exc()
			return Response(template_name="myclass/teacher.html")	
	def post(self,request):	
		try:
			data=dict(request.data.iterlists())
			print ">>>>>",data,data['designation'][0]
			desig=Designation.objects.get_or_create(name=data['designation'][0])
			user=MyUser.objects.get_or_create(email=data['email'][0])
			user[0].mobile=data['mobile'][0]
			user[0].types="teacher"
			user[0].name=data["tname"][0]
			print user
			user[0].save()
			# user.designation=desig
			course=Course.objects.filter(pk__in=data['course'])
			subject=Subject.objects.filter(pk__in=data['sub'])
			print ">>>>>>>>>>>",desig
			tchr=Teacher.objects.create(user=user[0],designation=desig[0])
			for i in course:
				tchr.course.add(i)
				tchr.save()
			for i in subject:
				tchr.subject.add(i)
				tchr.save()	
			return Response(template_name="myclass/admin.html")
		except Exception as e:
			traceback.print_exc()
			return Response(template_name="myclass/admin.html")	
class TeacherListView(APIView):
	renderer_classes=(TemplateHTMLRenderer,)
	def get(self,request):
		try:
			print "111111111111111111111"
			tchr=Teacher.objects.all()
			data=[]
			for i in tchr:
				sub=[]	
				for j in i.subject.all():
					sub.append({"name":j.name})
				course=[]
				for k in i.course.all():
					course.append({"name":k.name})
				data.append({"name":i.user.name,"email":i.user.email,"mobile":i.user.mobile,"designation":i.designation.name,"course":course,\
				"subject":sub})
			print data	
			return Response({"data":data},template_name="myclass/teacher1.html")		
		except Exception as e:
			return Response(template_name="myclass/teacher1.html")
class DashBoard(APIView):
	renderer_classes=(TemplateHTMLRenderer,)
	def get(self,request):
		try:
			student=MyUser.objects.filter(types="student")
			pending=student.filter(is_active=False).count()
			approved=student.filter(is_active=True).count()
			tchr=MyUser.objects.filter(types="teacher").count()
			course=Course.objects.all().count()
			return Response({'total':student.count(),'pending':pending,'approved':approved,'teacher':tchr,'course':course},template_name="myclass/dashboard.html")
		except Exception as e:
			traceback.print_exc()
			return Response(template_name="myclass/dashboard.html")								