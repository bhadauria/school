# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from account.models import *

# class School(models.Model):
# 	name=models.CharField(max_length=1000,null=True,blank=True)
# 	owner=models.ForeignKey(MyUser,null=True,blank=True)

# 	def __str__(self):
# 		return self.name
class Subject(models.Model):
	name=models.CharField(max_length=250,null=True,blank=True)

	def __str__(self):
		return self.name

class Course(models.Model):
	name=models.CharField(max_length=250,null=True,blank=True)
	# school=models.ForeignKey(School,null=True,blank=True)
	subject=models.ManyToManyField(Subject,blank=True)
	def __str__(self):
		return self.name
class Designation(models.Model):
	name=models.CharField(max_length=250,null=True,blank=True)

	def __str__(self):
		return self.name

class Teacher(models.Model):
	user=models.ForeignKey(MyUser,null=True,blank=True)
	designation=models.ForeignKey(Designation,null=True,blank=True)
	course=models.ManyToManyField(Course,blank=True)
	subject = models.ManyToManyField(Subject,blank=True)
	def __str__(self):
		return self.user.email